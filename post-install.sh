#!/usr/bin/env bash

DOCKER_PHP=$(docker ps | grep _php_ | cut -d' ' -f1)
DOCKER_NGINX=$(docker ps | grep _web_ | cut -d' ' -f1)
WEBROOT=public

echo "Installing composer dependencies"
docker exec -it ${DOCKER_PHP} sh -c "cd /var/www/html && php composer.phar install -a --ignore-platform-reqs"
echo

echo "Installing npm dependencies..."
docker exec -it ${DOCKER_NGINX} sh -c "cd /var/www/html && npm install"
echo ""
echo "All done! You can now execute ./webpack-dev-server.sh"
echo ""

echo "Installing Updater Endpoint"
echo
sh -c ./setup-updater.sh

echo "Linking Template Extension"
./link-extension.sh
echo ""

echo "Please enter your environment [local, staging, production]:"
read env

TARGET=./${WEBROOT}/typo3conf/LocalConfiguration-${env}.php
DESTINATION=./${WEBROOT}/typo3conf/LocalConfiguration.php
if [[ -f "$TARGET" ]]; then
    rm -f ${DESTINATION}
    cp ${TARGET} ${DESTINATION}
    echo "Copy LocalConfiguration.php"
else
    echo "No LocalConfiguration.php found"
fi

echo
echo "All done! You can now execute ./webpack-dev-server.sh"

TARGET=./${WEBROOT}/../config/sites/main/config-${env}.yaml
DESTINATION=./${WEBROOT}/../config/sites/main/config.yaml
if [[ -f "$TARGET" ]]; then
    rm -f ${DESTINATION}
    cp ${TARGET} ${DESTINATION}
    echo "Copy config.yaml for Site"
else
    echo "No config.yaml for Site found"
fi
