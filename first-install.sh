#!/usr/bin/env bash

echo "Removes old git..."
rm -rf .git
echo

echo "Initialize Git..."
echo ""
git init
echo ""
echo "Enter your remote repository"
echo "Ex: git@bitbucket.org:gutebotschafter/repository.git"
echo ""
read head
git remote add origin ${head}
git add .
git push origin master
git branch -m master production
git push origin production:refs/heads/production
git checkout -b staging
git push origin staging
echo ""

echo "Updates composer.phar..."
php composer.phar self-update
echo

docker-compose up -d

echo "Starting Post-Install Script"
echo ""
chmod +x post-install.sh
./post-install.sh
echo ""

echo "Installing Updater Endpoint"
echo
sh -c ./setup-updater.sh

rm -f first-install.sh
