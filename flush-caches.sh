#!/usr/bin/env bash

DOCKER_PHP=$(docker ps | grep _php_ | cut -d' ' -f1)

echo "Flush typo3 caches..."
docker exec -it ${DOCKER_PHP} sh -c "cd /var/www/html/vendor/bin && ./typo3cms cache:flush"
