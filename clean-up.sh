#!/usr/bin/env bash

echo "Clean up...";
rm -rf ./.git
rm -rf ./docker
rm -rf ./assets
rm -rf ./javascript
rm -rf ./node_modules
rm -rf ./webpack
rm -f ./.gitignore
rm -f ./.editorconfig
rm -f ./.eslintrc
rm -f ./.prettierrc
rm -f ./babel.config.js
rm -f ./create-access-token.sh
rm -f ./create-certificates.sh
rm -f ./docker-compose.yaml
rm -f ./docker-connect.sh
rm -f ./flush-caches.sh
rm -f ./package.json
rm -f ./package-lock.json
rm -f ./post-install.sh
rm -f ./postcss.config.js
rm -f ./setup-updater.sh
rm -f ./README.md
rm -f ./webpack-dev-server.sh
echo
