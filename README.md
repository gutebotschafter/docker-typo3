# typo3 with docker, composer and webpack-dev-server

## Table of Contents

  - [Installation](#installation)
  - [Usage](#usage)
    - [Container](#container)
    - [Dependencies](#dependencies)
    - [webpack](#webpack)
    - [Connect](#connect)
    - [Updater](#updater)
  - [Scripts](#scripts)
    - [First Install](#first-install)
    - [Flush Caches](#flush-caches)
    - [Link Extension](#link-extension)
    - [Post Deploy](#post-deploy)
    - [Post Install](#post-install)
    - [Sync Files](#sync-files)
  - [Deployment](#deployment)
  - [Update](#update)
  - [Database](#database)
  - [Debugging with xdebug](#debugging-with-xdebug)
  - [Codestyling](#codestyling)
    - [PHP](#php)
    - [Javascript](#javascript)
  - [Local tools](#local-tools)

## Installation

Please make sure, that you installed the [local tools](#local-tools).

Run `docker-compose build` to pulling the image and building the container.

## Usage

To start the docker-container run the command `docker-compose up`. (Shows log output)
If you want to start the container in the Background use `docker-compose up -d`.

### Container
To list the container use `docker ps`, you need the web_1 container name. Example:

| CONTAINER ID  | IMAGE        | COMMAND                | CREATED    | STATUS        | PORTS                  | NAMES                          |
| ------------- | ------------ | ---------------------- | -----------| --------------| -----------------------| -------------------------------|
| 1e313b2b7821  | nginx:latest | "nginx -g 'daemon of…" | 1 days ago | Up 34 minutes | 0.0.0.0:80->80/tcp,... | typo3-composer-webpack_web_1   |
| d705ab3eb7dc  | php:7-fpm    | "docker-php-entrypoi…" | 1 days ago | Up 34 minutes | 9000/tcp               | typo3-composer-webpack_php_1   |
| 7426189b0091  | phpmyadmin   | "/run.sh supervisord…" | 1 days ago | Up 34 minutes | 0.0.0.0:81->80/tcp     | typo3-composer-webpack_pma_1   |
| 40537d7c5415  | mysql:5.7    | "docker-entrypoint.s…" | 1 days ago | Up 34 minute  | 0.0.0.0:3306->3306/tcp | typo3-composer-webpack_mysql_1 |

### Dependencies

Run locally `./post-install.sh` to install all dependencies. 

### webpack

Execute the command `./webpack-dev-server.sh` to start the webpack-dev-server. The javascript entry point can be found in /src/app.js

### Connect

To connect to the container, you can use the script `./docker-connect.sh [container]`.

Replace the `[container]` with the Container Names, like `web`, `php` and so on...

### Updater

To install the updater, run easily `./setup-updater.sh`

## Scripts

There a hand full scripts to manage your cms and docker

### First install

This is the script you should execute after pulling this repo, try `./first-install.sh`. This scripts calls after execution the `./post-install.sh` script.

### Flush Caches

This script execute the cache:flush option from the helum typo console in your docker container. `./flush-caches.sh`

### Link Extension

Actually you must configure this script by yourself. This script creates a symlink of the template into your ext folder. Try: `./link-extension.sh`

This script is also called by the jenkins deployment!

### Post Deploy

This script is called after deployment by jenkins on the remote server.

### Post Install

This script installs the dependencies for composer and npm.

### Sync Files

Actually you must configure this script by yourself. It synchronizes the fileadmin with a remote server. Make sure that the remote Server has the ssh keys installed.
It's also used by the jenkins to update the staging environment.

## Deployment

If the production server doesn't have npm installed, remove the assets directory `/public/assets` from the gitignore and commit after you have run `npm run build` (for Production the assets).

## Update

Update composer modules only over composer and node modules over npm!

### Debugging with xdebug

You can debug over xdebug directly into the container. In order to debug the php on the container, you must setup a new local ip like
```
sudo ifconfig en0 alias 10.254.254.254 255.255.255.0
```
Now you can debug with xdebug on port **9001** with the ip **10.254.254.254**. This ist important for the container to share xdebug data to the host machine.

## Database

To configure tables from database, you can easily use **PhpMyAdmin**. You can find it on `http://localhost:81`.
Otherwise the login credentials are
* Username: **typo3**
* Password: **typo3**
* Host: **mysql**
* Port: **3306**

## Codestyling

### php

For php codestyling follow the [PSR/2 Standard](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md)

### Javascript

This package includes an ESLint config (**.eslintrc**, from [Airbnb](https://github.com/airbnb/javascript)) and a prettier config (**.prettierrc**), to format the javascript code.

Activate the Plugins in your favourite IDE.

## Local tools

Make sure, that you have installed the following tools:

- docker ce (https://hub.docker.com/editions/community/docker-ce-desktop-mac)
