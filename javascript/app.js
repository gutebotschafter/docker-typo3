import runtime from 'serviceworker-webpack-plugin/lib/runtime';
import './polyfills';
import './assets-loader';

const isDevelopment = process.env.NODE_ENV === 'development';

let refreshing = false;

if ('serviceWorker' in navigator && !isDevelopment) {
    navigator.serviceWorker.addEventListener('controllerchange', function () {
        if (refreshing) {
            return;
        }
    
        refreshing = true;
        window.location.reload();
    });

    window.addEventListener('load', () => runtime.register());
}

import { dummy } from './components/dummy';
// import loadChunks from './utils/load-chunks';

dummy();

/**
 * Activate to load chunks
 */
// loadChunks();
