#!/usr/bin/env bash

echo "Sets symlinks for composer binaries..."
rm -rf vendor/bin/*
cd vendor/bin
ln -s ../doctrine/dbal/bin/doctrine-dbal doctrine-dbal
chmod +x doctrine-dbal
ln -s ../typo3fluid/fluid/bin/fluid fluid
chmod +x fluid
ln -s ../nikic/php-parser/bin/php-parse php-parse
chmod +x php-parse
ln -s ../typo3/cms-cli/typo3 typo3
chmod +x typo3
ln -s ../helhum/typo3-console/typo3cms typo3cms
chmod +x typo3cms
cd ../..
echo

echo "Links template..."
chmod +x link-extension.sh
./link-extension.sh
echo

echo "Executes typo3 install post scripts..."
./vendor/bin/typo3cms install:fixfolderstructure
./vendor/bin/typo3cms install:generatepackagestates
echo

echo "Flushes the cache..."
chmod +x ./vendor/bin/typo3cms
./vendor/bin/typo3cms cache:flush
echo
