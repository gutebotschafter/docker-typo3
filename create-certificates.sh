#!/usr/bin/env bash

# Site
openssl req -newkey rsa:2048 -nodes -keyout ./docker/nginx/certificates/local.docker.key -x509 -days 365 -out ./docker/nginx/certificates/local.docker.crt -subj "/C=DE/ST=Haltern/L=Haltern/O=GuteBotschafter/OU=GuteBotschafter/CN=local.docker"

# Webpack Dev Server
openssl req -new -sha256 -nodes -keyout ./docker/nginx/certificates/wds.key -x509 -days 365 -out ./docker/nginx/certificates/wds.crt -subj "/C=DE/ST=Haltern/L=Haltern/O=GuteBotschafter/OU=GuteBotschafter/CN=localhost"
openssl x509 -in ./docker/nginx/certificates/wds.crt -out ./docker/nginx/certificates/wds.pem -outform PEM
