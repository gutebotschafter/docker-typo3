#!/bin/bash

PORT=22 # SSH PORT ON PRODUCTION SYSTEM
SOURCES="" # PRODUCTION SYSTEM => ex.: user@host:/path
TARGET="" # STAGING OR LOCAL SYSTEM

RSYNCCONF="-rtpgo"

rsync ${RSYNCCONF} --progress --delete -e "ssh -p $PORT" ${SOURCES} ${TARGET}

exit 0
