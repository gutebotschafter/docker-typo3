#!/usr/bin/env bash

DOCKER_PHP=$(docker ps | grep _php_ | cut -d' ' -f1)
DOCKER_NGINX=$(docker ps | grep _web_ | cut -d' ' -f1)
WEBROOT=web

echo "Installing npm dependencies..."
docker exec -it ${DOCKER_NGINX} sh -c "cd /var/www/html && npm install -g gulp && npm install"
echo

echo "Installing composer dependencies"
docker exec -it ${DOCKER_PHP} sh -c "cd /var/www/html && php composer.phar install -a --ignore-platform-reqs"
echo

echo "Installing template npm dependencies"
TEMPLATE=$(find web/typo3conf/ext -maxdepth 1 -type d -name '*_template' -print -quit)
docker exec -it ${DOCKER_NGINX} sh -c "cd /var/www/html/${TEMPLATE} && npm install"
echo

echo "Installing Updater Endpoint"
echo
sh -c ./setup-updater.sh

echo "Please enter your environment [local, staging, production]:"
read env

TARGET=./${WEBROOT}/typo3conf/LocalConfiguration-${env}.php
DESTINATION=./${WEBROOT}/typo3conf/LocalConfiguration.php
if [[ -f "$TARGET" ]]; then
    rm -f ${DESTINATION}
    cp ${TARGET} ${DESTINATION}
    echo "Copy LocalConfiguration.php"
else
    echo "No LocalConfiguration.php found"
fi

echo
echo "All done! You can now execute ./webpack-dev-server.sh"
