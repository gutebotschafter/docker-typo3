const webpack = require('webpack');
const merge = require('webpack-merge');
const WebpackBar = require('webpackbar');
const { join, resolve } = require('path');
const common = require('./common');

module.exports = merge(common, {
    entry: {
        app: [
            join(__dirname, '../javascript/app'),
            resolve(__dirname, '../assets/styles/development.scss')
        ],
        criticals: resolve(__dirname, '../assets/styles/criticals.scss'),
    },
    watchOptions: {
        poll: true
    },
    mode: 'development',
    devtool: 'cheap-eval-module-source-map',
    plugins: [
        new WebpackBar(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new webpack.DefinePlugin({
            'webpack.env.CACHE_KEY': JSON.stringify(`sw-cache-${Date.now()}`)
        })
    ]
});
