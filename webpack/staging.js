const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./common');
const { join, resolve } = require('path');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = merge(common, {
    entry: {
        app: [
            join(__dirname, '../javascript/app'),
            resolve(__dirname, '../assets/styles/general.scss')
        ],
        criticals: resolve(__dirname, '../assets/styles/criticals.scss'),
    },
    mode: 'production',
    devtool: 'source-map',
    plugins: [
        new webpack.DefinePlugin({
            'webpack.env.CACHE_KEY': JSON.stringify(`sw-cache-${Date.now()}`)
        }),
        new TerserPlugin({
            sourceMap: true,
            parallel: true
        }),
        new webpack.optimize.ModuleConcatenationPlugin(),
    ]
});
