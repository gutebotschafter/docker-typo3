const merge = require('webpack-merge');
const prod = require('./prod');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = merge(prod, {
    stats: {
        assets: false,
        cached: false,
        cachedAssets: false,
        children: false,
        chunks: true,
        hash: false,
        modules: false,
        maxModules: 3,
        performance: false,
        reasons: false,
        source: false,
        version: false,
        warnings: false
    },
    plugins: [
        new BundleAnalyzerPlugin({
            analyzerMode: 'server',
            analyzerHost: 'localhost',
            analyzerPort: 8888
        })
    ]
});
