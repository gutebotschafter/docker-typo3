const webpack = require('webpack');
const merge = require('webpack-merge');
const WebpackBar = require('webpackbar');
const { join, resolve } = require('path');
const common = require('./common');

module.exports = merge(common, {
    entry: {
        app: [
            'webpack-dev-server/client?http://0.0.0.0:8080',
            join(__dirname, '../javascript/app'),
            resolve(__dirname, '../assets/styles/development.scss')
        ],
        criticals: resolve(__dirname, '../assets/styles/criticals.scss'),
    },
    devServer: {
        hot: true,
        historyApiFallback: true,
        host: '0.0.0.0',
        port: 8080,
        stats: {
            colors: true
        },
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': '*'
        },
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000
        }
    },
    watchOptions: {
        poll: true
    },
    mode: 'development',
    devtool: 'cheap-eval-module-source-map',
    plugins: [
        new WebpackBar(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin()
    ]
});
